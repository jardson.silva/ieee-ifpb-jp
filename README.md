# IEEE-IFPB-JP

* Sistema de cadastro

Faça um programa simples para realizar um cadastro de pessoas recebendo dados via standard input. O programa deve:

    - Cadastrar pessoas
    - Consultar pessoas pelo CPF
    - Atualizar cadastro
    - Excluir cadastro

Cada registro deve conter os seguintes dados pessoas:
    - CPF (único para cada usuário)
    - Nome completo
    - Telefone
    - Endereço

* Menu principal

Crie uma tela de boas-vindas com um menu informando ao usuário as opções do programa:
Boas vindas ao nosso sistema:


1 - Inserir usuário
2 - Excluir usuário
3 - Atualizar usuário
4 - Informações de um usuário
5 - Informações de todos os usuários
6 - Sair