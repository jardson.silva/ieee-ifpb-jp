# Dicionário de Teste
user_test = {
    # Chave
    "123.456.789-10":{
    "nome": "João Carvalho",
    "telefone": "012341234",
    "email": "assimdisseotio@gmail.com",
    "endereco":"ESTAMOS NO BRASILL!!"
    },
    "824.445.955-35":{
        "nome": "Jardson Silva",
        "telefone": "zxxxxxxxx",
        "email": "jsilva@gmail.com",
        "endereco":"Konohamaru"
    }
}

# Função para Atualizar os dados do usuário
def atualizar_usuarios(cpf, nome, telefone, email, endereco):
    if cpf in user_test:
        user_test[cpf]["nome"] = nome
        user_test[cpf]["telefone"] = telefone 
        user_test[cpf]["email"] = email
        user_test[cpf]["endereco"] = endereco
        print("Dados do usuário atualizados com sucesso!")
    else:
          print("CPF não encontrado. Não é possível atualizar os dados.")
          
          # Condição para inserir um novo usuário
          criar_novo = input("Deseja criar um novo usuário? (S/N)") 
          if criar_novo.lower() == "s" or criar_novo.lower() == "sim":
            user_test[cpf] = {"nome": nome, "idade": idade, "email": email}
            print("Novo usuário criado com sucesso!")  
            
# Função para exibir os dados de um usuário
def exibir_informacoes(cpf):
    if cpf in user_test:
        usuario = user_test[cpf]
        print(f"CPF: {cpf}")
        print(f"Nome: {usuario['nome']}")
        print(f"Telefone: {usuario['telefone']}")
        print(f"Email: {usuario['email']}")
        print(f"Endereço: {usuario['endereco']}")
    else:
        print("CPF não encontrado.")

# Menuzinho 
while True:
    print("+----------- Menu -----------+")
    print("1) Atualizar dados de usuário")
    print("2) Exibir dados de usuário")
    print("3) Sair")
    print("+----------------------------+")

    opcao = input("Digite a opção desejada: ")

    if opcao == "1":
        cpf = input("Insira o CPF do usuário que você quer atualizar as informações: ")
        nome = input("Insira o novo nome: ")
        telefone = input("Insira o novo telefone: ")
        email = input("Insira o novo e-mail: ")
        endereco = input("Insira o novo endereço: ")

        # Chamada da função atualizar_usuarios()
        atualizar_usuarios(cpf, nome, telefone, email, endereco)
    elif opcao == "2":
        cpf = input("Insira o CPF do usuário que você quer verificar as informações: ")
        
        # Chamada da função exibir_informacoes()
        exibir_informacoes(cpf)
    elif opcao == "3":
        break
    else:
        print("Opção inválida. Tente novamente.")